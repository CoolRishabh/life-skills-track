# Energy Management

1. What are the activities you do that make you relax - Calm quadrant?
   - Engaging in activities such as walking, exercise, or listening to calming music helps me unwind and feel refreshed in the Calm quadrant.
2. When do you find getting into the Stress quadrant?
   - The Stress quadrant is where I often find myself on challenging tasks, or unexpected setbacks where i have to be on screen for a long time.
3. How do you understand if you are in the Excitement quadrant?
   - Increased energy level.
   - Eagerness to take on new challenges.
   - Looking forward to what's coming with a sense of excitement.
4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points.
   - Sleep is important for our health and how well we perform.
   - Not getting enough sleep can make it tough to concentrate and remember things.
   - When we sleep, our brains sort through memories and remove toxins.
   - Not getting sufficient sleep can raise the risk of health problems like heart disease and obesity.
   - To sleep well, it's important to have a regular bedtime and make sure your sleep environment is comfy.
5. What are some ideas that you can implement to sleep better?
   - Maintain consistent sleep time.
   - Ensure the bedroom is comfortable.
   - Avoid screen-usage, like phones or computers before bed.
   - Don't eat heavy meals or drink caffeine close to bedtime.
   - Practise stress management techniques deep breathing or meditation exercises.
6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
    - Doing regular exercise is really good for your brain.
    - When you exercise, your brain grows new cells, which makes you smarter.
    - It also makes you feel happier by releasing chemicals that make you feel good.
    - Exercise helps keep your brain healthy as you get older, lowering the chances of memory problems.
    - Even a little bit of exercise, like going for a walk, can make a big difference for your brain.
7. What are some steps you can take to exercise more?
   - Begin with simple activities, like a short walk or stretching at home.
   - Select exercises that you enjoy.
   - Exercise with friends or join a class to make it more enjoyable.
   - Incorporate exercise into your daily routine by scheduling it like any other appointment and finding opportunities to be active, like taking the stairs or walking instead of driving.
