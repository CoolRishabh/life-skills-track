# Tiny habits

1. In this video (https://www.youtube.com/watch?v=AdKUJxjn-R8), what was the most interesting story or idea for you?
   - Tiny habits are tiny, easy actions that can lead to big changes in behavior.
   - For example, doing one push-up every morning might not seem like much, but over time, it can make you stronger.

2. How can you use B = MAP to make making new habits easier? What are M, A and P?
    - Motivation (M): Your desire or willingness to do something.
    - Ability (A): Your capacity to perform a behavior.
    - Prompt (P): A cue or trigger that reminds you to do something.
    - Make the habit small and achievable. The smaller the behavior, the less motivation you'll need and the more likely you are to stick with it.
    - Identify an action prompt by pairing your new habit with something you already do.
    - Celebrate your wins, no matter how small. It boosts motivation and increases the likelihood of repeating the behavior.
3. Why it is important to "Shine" or Celebrate after each successful completion of habit?
   - Celebrating success tells your brain "good job!" and encourages repeating the behavior.
   - It helps turn actions into habits by reinforcing them.
   - This aligns with how our minds work.
4. What was the most interesting story or idea from 1% Better Every Day by James Clear?
    - Changing our perspective can transform experiences.
    - Small improvements can lead to remarkable results over time.
    - The story of the man who saw illness as a gift was eye-opening.
    - It demonstrated how shifting mindset promotes personal growth and resilience.

5. How does "Atomic Habits" by James Clear view identity?
     - Dynamic Identity: Identity is portrayed as constantly evolving.
     - Influenced by External Factors: External influences like culture and relationships shape identity.
     - Intersectionality: Different aspects of identity intersect and influence one another.
     - Complexity: Identity is seen as fluid and complex, defying simple categorization.
  
6. Write about the book's perspective on how to make a habit easier to do?
      - Start Tiny.
      - Stay Consistent.
      - Reflect and learn.
      - Enjoy small wins.
7. Book's perspective on how to make a habit harder to do
   - Make the habit harder by adding obstacles or making it mentally or physically demanding.
   - Switch up your usual environment to somewhere less suitable for the habit, making it less convenient.
   - Introduce delays or obstacles before you can do the habit, reducing its immediacy.
   - Create negative consequences for doing the habit, decreasing your desire to engage in it.
  
8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

    Daily weight-gaining workout routine
   - Make it accessible: Keep your weights or resistance bands in a visible area where you'll remember to use them.
   - Make the Habit More Attractive: Focus on exercises that you enjoy and that target muscle growth, such as weightlifting or bodyweight exercises.
   - Make the Habit Easier: Start with a manageable weight and gradually increase the intensity and duration of your workouts.
   - Celebrate: Treat yourself to a nutritious post-workout meal or snack to support muscle recovery and growth.
9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
    Spending too much time on social media
    - Make it inaccessible: Disable notifications or set designated times for social media use.
    - Make it unattractive: Avoid addictive apps and consider the negative effects of excessive use. Opt for generic apps and stick to them.
    - Make the habit harder: Keep your phone out of reach or on silent mode during certain activities or at specific times daily.
    - Shift focus: Redirect time spent on social media to more productive tasks.