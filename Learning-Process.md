# Learning process
I will apply the 80/20 principle, also known as the Pareto Principle, which suggests that roughly 80% of results come from 20% of efforts. In the context of learning, this means that focusing on the most important and impactful actions can lead to significant improvements in your learning process.
1. **Identify key concept**: Find the 20% of the concepts that are most important to understand a subject.
2. **Focus on High-Impact resources**: Spending time on resources that provide the most value. It could be Text-books, ChatGPT or YouTube tutorials.
3. **Apply the Feynman Technique**: Use the Feynman Technique to explain complex concepts in simple terms. Teaching others or writing out the explanations helps to strengthen your understanding and identify areas where you need more works.
4. **Seek Feedback and Adjust**: Regularly evaluate your learning progress and seek feedback from mentors, peers, or instructors.
5. **Prioritize Deep Understanding**: Instead of trying to memorize facts or information, focus on developing a deep understanding of the concepts.
