
# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

1. Verbal harassment:

     - Spreading rumours about a person's personal or sexual life

     - Comments about clothing.

     - Unwelcome sexual advances.

     - Requests for sexual favours.

  

2. Visual harassment:

   - Obscene posters.

   - Screensavers

  

2. Physical harassment:

   - Inappropriate touching.

   - Sexual gesturing.

  
  

## In case of facing or witnessing such behaviour:

1. Report it immediately to HR or a trusted authority figure.

2. Document the incident with as much detail as possible.

3. Offer support to the victim and encourage them to seek help.
