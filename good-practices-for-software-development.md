# Good-practices-for-software-development.md

1. Which point(s) were new to you?
   - We can track our time using app like "Boosted" to improve productivity.
   - Keeping you video turned-on during meeting can improve the relationship with your team and enhance communication.

2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
   -  Tracking my time using app like Boosted to improve productivity.
   -  Eating good food and exercise daily.