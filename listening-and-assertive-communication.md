
# Listening and Assertive Communication

1. What are the steps/strategies to do Active Listening?
   - Don't get distracted on your own thoughts.
   - Dont't interrupt when they are speaking.
   - Use door openers like:
     - Can you tell me more about it?
     - I'd really love to hear what you are thinking?
   - Show that you are listening with your body language.
   - Paraphrase what other are saying.

2. According to Fisher's model, what are the key points of Reflective Listening? 
    - Listen more.
    - Respond to what is personal rather than impersonal.
    - Paraphrase and clarify what other has said.
    - Try to understand the emotion in what the other is saying, not just the facts or ideas.
    - Responding with empathy, not with indiffernce or fake concern.

3. What are the obstacles in your listening process?
   - Internal thoughts.
   - Assuming I already know what the speaker is going to say without fully listening.
   - When feeling rushed or pressed for time.
   -  Not providing or receiving feedback during communication

4. What can you do to improve your listening?
   - Cultivate Curiosity.
   - Reflect on my Listening Habits.
   - Show Empathy.
   - Paraphrase and Summarize.
   - Ask Clarifying questions on any points that are unclear or ambiguous.

5. When do you switch to Passive communication style in your day to day life?
   - Lack of Self-Confidence.
   - Avoiding Conflict.
   - Cultural Norms.
   - Lack of Assertiveness Training.

6. When do you switch into Aggressive communication styles in your day to day life?
   - Defending Boundaries.

7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
   - Reacting to Criticism.
   - Expressing Disapproval.
   - Handling Conflict Avoidance.


8. How can you make your communication assertive?
   - Maintain Eye Contact.
   - Be Direct and Specific.
   - Practice Self-Awareness.
   - Stand Firm on Boundaries.
   - Exress Confidence.
   - Practice Active Listening.