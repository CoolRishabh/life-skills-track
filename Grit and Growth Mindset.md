# Grit and Growth Mindset
1. Paraphrase (summarize) the video https://www.youtube.com/watch?v=H14bBuluwB8 in a few (1 or 2) lines. 
   - How successful you will be on life does not depend on talent, IQ, physical health or social background, success depends on your continued effort (for a long time maybe years) despite the diffuclties, failures or opposition.


2. Paraphrase (summarize) the video ( https://www.youtube.com/watch?v=75GFzikmRY0) in a few (1 or 2) lines in your own words.
   - The video speaks about the difference between a growth (successful)  and fixed mindset (less likely to be successful).
   - People with a fixed mindset think that talent is inborn and can't be nurtured. When they are faced with difficulty, they are most likely to give up because they are not aware of how to overcome it.
   - People with a growth mindset think that talent depends on the hard work a person puts into building it. When faced with difficulty, they try their best to overcome it. Even when they fail, they strive to improve


3. What is the Internal Locus of Control? What is the key point in the video?
    - Focus on things you can control.
    - Don't rely on external motivation, they are short term.

4. What are the key points mentioned by speaker to build growth mindset ( https://www.youtube.com/watch?v=9DVdclX6NzY)
   - Trust your ability to learn and improve.
   - Challenge your assumptions about your capabilities.
   - Design your personal curriculum for learning and development.
   - Embrace struggles as opportunities for growth.

5. What are your ideas to take action and build Growth Mindset?
   - Achieving mastery is a journey and takes time and effort to build.
   - Learn from your mistakes.
   - Persist through challenges.